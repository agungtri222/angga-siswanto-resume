<?php

return [

    'file_attachments' => [
     
        // Firts Banner Images
        'first_front_banner' => \App\FileAttachments\FirstFrontBannerAttachment::class,

        // Second Banner Images
        'second_front_banner' => \App\FileAttachments\SecondFrontBannerAttachment::class,

        // testimonials Images
        'testimonial_image' => \App\FileAttachments\TestimonialImageAttachment::class,

        // portfolio images
        'portfolio_images' => \App\FileAttachments\PortfolioImageAttachment::class,

        // CV document
        'cv_document' => \App\FileAttachments\CvDocumentAttachment::class,

        // portfolio document
        'portfolio_document' => \App\FileAttachments\PortfolioDocumentAttachment::class,
    ]

];