<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\GeneralSetting;

class GeneralSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GeneralSetting::truncate();
        
        GeneralSetting::create([
            'name' => 'Angga Siswanto',
            'email' => 'tr.angga.s@gmail.com',
            'phone' => '+62819825282',
            'headquarter' => 'Rungkut, Wonorejo, Surabaya 60296',
            'about_me' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, laborum quaerat hic corrupti ea blanditiis nemo ratione cupiditate necessitatibus dolor, possimus repudiandae exercitationem est adipisci totam consequatur eligendi excepturi assumenda.',
            'linkedin_url' => 'https://linkedin.com/in/anggasitako/',
        ]);
    }
}
