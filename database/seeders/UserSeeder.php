<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        
        User::create([
            'name' => 'Angga Siswanto',
            'email' => 'tr.angga.s@gmail.com',
            'password' => bcrypt('password')
        ]);
    }
}
