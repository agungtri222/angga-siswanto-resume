<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralSettingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_settings', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('headquarter');
            $table->text('about_me');
            $table->string('linkedin_url')->nullable()->default('https://linkedin.com');
            $table->string('facebook_url')->nullable()->default('https://facebook.com');
            $table->string('instagram_url')->nullable()->default('https://instagram.com');
            $table->string('youtube_url')->nullable()->default('https://youtube.com');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_settings');
    }
}
