<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', App\Http\Controllers\FrontController::class)->name('front');

Auth::routes();

Route::apiResource('attachments', \App\Http\Controllers\AttachmentController::class)->only(['store', 'show', 'destroy']);

Route::group([ 'prefix' => 'admin', 'middleware' => 'auth' ], function() {

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('admin');
    
    
    Route::resource('generalSettings', App\Http\Controllers\GeneralSettingController::class);
    
    
    Route::resource('myExpertises', App\Http\Controllers\MyExpertiseController::class);
    
    
    Route::resource('myExperiences', App\Http\Controllers\MyExperienceController::class);
    
    
    Route::resource('education', App\Http\Controllers\EducationController::class);
    
    
    Route::resource('skills', App\Http\Controllers\SkillController::class);
});




Route::resource('testimonials', App\Http\Controllers\TestimonialController::class);


Route::resource('portfolios', App\Http\Controllers\PortfolioController::class);
