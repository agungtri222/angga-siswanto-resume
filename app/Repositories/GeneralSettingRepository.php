<?php

namespace App\Repositories;

use App\Models\GeneralSetting;
use App\Repositories\BaseRepository;

/**
 * Class GeneralSettingRepository
 * @package App\Repositories
 * @version August 5, 2022, 1:15 am UTC
*/

class GeneralSettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GeneralSetting::class;
    }
}
