<?php

namespace App\Repositories;

use App\Models\MyExperience;
use App\Repositories\BaseRepository;

/**
 * Class MyExperienceRepository
 * @package App\Repositories
 * @version August 5, 2022, 2:05 am UTC
*/

class MyExperienceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MyExperience::class;
    }
}
