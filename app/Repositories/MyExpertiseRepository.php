<?php

namespace App\Repositories;

use App\Models\MyExpertise;
use App\Repositories\BaseRepository;

/**
 * Class MyExpertiseRepository
 * @package App\Repositories
 * @version August 5, 2022, 1:59 am UTC
*/

class MyExpertiseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MyExpertise::class;
    }
}
