<?php

namespace App\Repositories\Attachment\Contracts;

interface FileAttachment
{
    /**
     * Get disk
     *
     * @return string
     */
    public function disk();

    /**
     * Get directory
     *
     * @return string
     */
    public function directory();

    /**
     * Get validation rules
     *
     * @return string|array
     */
    public function rules();
}