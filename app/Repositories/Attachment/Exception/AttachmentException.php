<?php

namespace App\Repositories\Attachment\Exception;

use App\Repositories\Attachment\Contracts\FileAttachment;
use Exception;

class AttachmentException extends Exception
{
    /**
     * Display message for unmapped exception
     *
     * @param  string  $key
     * @return AttachmentException
     */
    public static function unmapped(string $key)
    {
        return new static("The {$key} is not registered in file attachment map");
    }

    /**
     * Display message for invalid instance exception
     *
     * @param  object  $object
     * @return AttachmentException
     */
    public static function invalidInstance($object)
    {
        $className = get_class($object);

        return new static("The {$className} must implements ". FileAttachment::class);
    }
}