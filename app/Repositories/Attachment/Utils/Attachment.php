<?php

namespace App\Repositories\Attachment\Utils;

trait Attachment
{
    /**
     * Get disk
     *
     * @return string
     */
    public function disk()
    {
        return 'public';
    }

    /**
     * Get directory
     *
     * @return string
     */
    public function directory()
    {
        return 'attachments';
    }
}