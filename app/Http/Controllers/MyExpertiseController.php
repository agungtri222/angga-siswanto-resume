<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMyExpertiseRequest;
use App\Http\Requests\UpdateMyExpertiseRequest;
use App\Repositories\MyExpertiseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MyExpertiseController extends AppBaseController
{
    /** @var MyExpertiseRepository $myExpertiseRepository*/
    private $myExpertiseRepository;

    public function __construct(MyExpertiseRepository $myExpertiseRepo)
    {
        $this->myExpertiseRepository = $myExpertiseRepo;
    }

    /**
     * Display a listing of the MyExpertise.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $myExpertises = $this->myExpertiseRepository->all();

        return view('my_expertises.index')
            ->with('myExpertises', $myExpertises);
    }

    /**
     * Show the form for creating a new MyExpertise.
     *
     * @return Response
     */
    public function create()
    {
        return view('my_expertises.create');
    }

    /**
     * Store a newly created MyExpertise in storage.
     *
     * @param CreateMyExpertiseRequest $request
     *
     * @return Response
     */
    public function store(CreateMyExpertiseRequest $request)
    {
        $input = $request->all();

        $myExpertise = $this->myExpertiseRepository->create($input);

        Flash::success('My Expertise saved successfully.');

        return redirect(route('myExpertises.index'));
    }

    /**
     * Display the specified MyExpertise.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $myExpertise = $this->myExpertiseRepository->find($id);

        if (empty($myExpertise)) {
            Flash::error('My Expertise not found');

            return redirect(route('myExpertises.index'));
        }

        return view('my_expertises.show')->with('myExpertise', $myExpertise);
    }

    /**
     * Show the form for editing the specified MyExpertise.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $myExpertise = $this->myExpertiseRepository->find($id);

        if (empty($myExpertise)) {
            Flash::error('My Expertise not found');

            return redirect(route('myExpertises.index'));
        }

        return view('my_expertises.edit')->with('myExpertise', $myExpertise);
    }

    /**
     * Update the specified MyExpertise in storage.
     *
     * @param int $id
     * @param UpdateMyExpertiseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMyExpertiseRequest $request)
    {
        $myExpertise = $this->myExpertiseRepository->find($id);

        if (empty($myExpertise)) {
            Flash::error('My Expertise not found');

            return redirect(route('myExpertises.index'));
        }

        $myExpertise = $this->myExpertiseRepository->update($request->all(), $id);

        Flash::success('My Expertise updated successfully.');

        return redirect(route('myExpertises.index'));
    }

    /**
     * Remove the specified MyExpertise from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $myExpertise = $this->myExpertiseRepository->find($id);

        if (empty($myExpertise)) {
            Flash::error('My Expertise not found');

            return redirect(route('myExpertises.index'));
        }

        $this->myExpertiseRepository->delete($id);

        Flash::success('My Expertise deleted successfully.');

        return redirect(route('myExpertises.index'));
    }
}
