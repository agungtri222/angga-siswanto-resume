<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Attachment;

class AttachmentController extends Controller
{
    /**
     * @var Attachment $attachment
     */
    protected $attachment;

    /**
     * Attachment Resource Constructor
     * 
     * @param Attachment $attachment
     */
    public function __construct(Attachment $attachment)
    {
        $this->attachment = $attachment;

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! $request->has('file_attachment')) {
            $data = [
                'message' => 'Data yang diberikan tidak tepat.',
                'errors'  => [
                    'file_attachment' => [
                        \Lang::get('validation.required', [
                            'attribute' => 'lampiran berkas',
                        ]),
                        \Lang::get('validation.string', [
                            'attribute' => 'lampiran berkas',
                        ]),
                    ],
                ],
            ];

            return response()->json($data, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $fileAttachment = \App::make($request->file_attachment);

        $request->validate([
            'attachable_type' => 'required|string',
            'attachable_id'   => 'nullable|numeric',
            'attachment'      => $fileAttachment->rules(),
        ], [], [
            'attachable_type' => 'attachment type',
            'attachable_id'   => 'attachment id',
            'attachment'      => 'attachment',
        ]);

        $file = $request->file('attachment');

        if (! $path = $file->store($fileAttachment->directory(), $fileAttachment->disk())) {
            return response()->json([
                'message' => 'Failed to store the attachment.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $data = [
            'attachable_type' => $request->attachable_type,
            'file_attachment' => $request->file_attachment,
            'name'            => $file->getClientOriginalName(),
            'path'            => $path,
            'size'            => $file->getSize(),
            'type'            => $file->getClientMimeType()
        ];

        if (isset($request->attachable_id)) {
            $data['attachable_id'] = $request->attachable_id;
        }

        $attribute = Attachment::create($data);

        return response()->json([
            'message' => 'Attachment Created.',
            'data'    => $attribute,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attribute = Attachment::find($id);

        return response()->json([
            'data' => $attribute
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = Attachment::where('id', $id)->delete();

        return response()->json([
            'message' => 'Attachment deleted.',
            'deleted' => $deleted,
        ], Response::HTTP_NO_CONTENT);
    }
}