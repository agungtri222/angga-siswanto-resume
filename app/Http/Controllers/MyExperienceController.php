<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMyExperienceRequest;
use App\Http\Requests\UpdateMyExperienceRequest;
use App\Repositories\MyExperienceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MyExperienceController extends AppBaseController
{
    /** @var MyExperienceRepository $myExperienceRepository*/
    private $myExperienceRepository;

    public function __construct(MyExperienceRepository $myExperienceRepo)
    {
        $this->myExperienceRepository = $myExperienceRepo;
    }

    /**
     * Display a listing of the MyExperience.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $myExperiences = $this->myExperienceRepository->all();

        return view('my_experiences.index')
            ->with('myExperiences', $myExperiences);
    }

    /**
     * Show the form for creating a new MyExperience.
     *
     * @return Response
     */
    public function create()
    {
        return view('my_experiences.create');
    }

    /**
     * Store a newly created MyExperience in storage.
     *
     * @param CreateMyExperienceRequest $request
     *
     * @return Response
     */
    public function store(CreateMyExperienceRequest $request)
    {
        $input = $request->all();

        $myExperience = $this->myExperienceRepository->create($input);

        Flash::success('My Experience saved successfully.');

        return redirect(route('myExperiences.index'));
    }

    /**
     * Display the specified MyExperience.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $myExperience = $this->myExperienceRepository->find($id);

        if (empty($myExperience)) {
            Flash::error('My Experience not found');

            return redirect(route('myExperiences.index'));
        }

        return view('my_experiences.show')->with('myExperience', $myExperience);
    }

    /**
     * Show the form for editing the specified MyExperience.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $myExperience = $this->myExperienceRepository->find($id);

        if (empty($myExperience)) {
            Flash::error('My Experience not found');

            return redirect(route('myExperiences.index'));
        }

        return view('my_experiences.edit')->with('myExperience', $myExperience);
    }

    /**
     * Update the specified MyExperience in storage.
     *
     * @param int $id
     * @param UpdateMyExperienceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMyExperienceRequest $request)
    {
        $myExperience = $this->myExperienceRepository->find($id);

        if (empty($myExperience)) {
            Flash::error('My Experience not found');

            return redirect(route('myExperiences.index'));
        }

        $myExperience = $this->myExperienceRepository->update($request->all(), $id);

        Flash::success('My Experience updated successfully.');

        return redirect(route('myExperiences.index'));
    }

    /**
     * Remove the specified MyExperience from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $myExperience = $this->myExperienceRepository->find($id);

        if (empty($myExperience)) {
            Flash::error('My Experience not found');

            return redirect(route('myExperiences.index'));
        }

        $this->myExperienceRepository->delete($id);

        Flash::success('My Experience deleted successfully.');

        return redirect(route('myExperiences.index'));
    }
}
