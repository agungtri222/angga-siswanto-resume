<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\GeneralSetting;
use App\Models\MyExperience;
use App\Models\MyExpertise;
use App\Models\Skill;
use App\Models\Education;
use App\Models\Testimonial;
use App\Models\Portfolio;

class FrontController extends Controller
{
    public function __invoke()
    {
        $generalSetting = GeneralSetting::first();
        $myExperiences = MyExperience::all();
        $myExpertises = MyExpertise::all();
        $skills = Skill::all();
        $educations = Education::all();
        $testimonials = Testimonial::all();
        $portfolios = Portfolio::with('portfolioImage')->get();
        return view('layouts.front.index', compact('generalSetting', 'myExperiences', 'myExpertises', 'skills', 'educations', 'testimonials', 'portfolios'));
    }
}
