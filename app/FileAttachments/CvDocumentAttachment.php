<?php

namespace App\FileAttachments;

use App\Repositories\Attachment\Contracts\FileAttachment;
use App\Repositories\Attachment\Utils\Attachment;

class CvDocumentAttachment implements FileAttachment
{
    use Attachment;

    /**
     * Get validation rules
     *
     * @return string|array
     */
    public function rules()
    {
        return [
            'file', 'max:2048', 'mimes:pdf',
        ];
    }

    /**
     * @inheritDoc
     */
    public function directory()
    {
        return 'front/cv';
    }
}