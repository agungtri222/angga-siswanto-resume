<?php

namespace App\FileAttachments;

use App\Repositories\Attachment\Contracts\FileAttachment;
use App\Repositories\Attachment\Utils\Attachment;

class PortfolioDocumentAttachment implements FileAttachment
{
    use Attachment;

    /**
     * Get validation rules
     *
     * @return string|array
     */
    public function rules()
    {
        return [
            'file', 'max:10000', 'mimes:pdf',
        ];
    }

    /**
     * @inheritDoc
     */
    public function directory()
    {
        return 'front/portfolios';
    }
}