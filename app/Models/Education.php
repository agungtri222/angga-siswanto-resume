<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Education
 * @package App\Models
 * @version August 6, 2022, 12:37 pm UTC
 *
 */
class Education extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'education';
    

    protected $dates = [
        'deleted_at',
        'start_date',
        'end_date',
    ];



    public $fillable = [
        'name',
        'degree',
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be casted to native types.
     * 
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|min:3',
        'degree' => 'required|string|min:3',
        'start_date' => 'required|date',
        'end_date' => 'date|after:start_date',
    ];

    
}
