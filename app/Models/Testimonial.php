<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Testimonial
 * @package App\Models
 * @version August 9, 2022, 9:05 am UTC
 *
 */
class Testimonial extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'testimonials';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'quote',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|min:3',
        'quote' => 'required|string|min:5|max:255',
    ];
    
    public function testimonialImage()
    {
        return $this->morphOne(Attachment::class, 'attachable')
            ->fileAttachment('testimonial_image');
    }
    
}
