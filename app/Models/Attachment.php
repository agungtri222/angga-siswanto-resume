<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Attachment extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $table = 'attachments';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'attachable_type',
        'attachable_id',
        'file_attachment',
        'name',
        'path',
        'size',
        'type',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'url',
    ];

    /**
     * Scope a query to only include the specified file attachment
     *
     * @param  Builder  $query
     * @param  string  $fileAttachment
     * @return Builder
     */
    public function scopeFileAttachment(Builder $query, string $fileAttachment)
    {
        return $query->where('file_attachment', $fileAttachment);
    }

    /**
     * Attachment morph to attachable
     * 
     * @return MorphTo
     */
    public function attachable()
    {
        return $this->morphTo();
    }

    /**
     * Get attachment url
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        $fileAttachment = \App::make($this->file_attachment);

        return \Storage::disk($fileAttachment->disk())->url($this->path);
    }
}
