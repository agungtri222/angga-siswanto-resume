<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Skill
 * @package App\Models
 * @version August 6, 2022, 12:43 pm UTC
 *
 */
class Skill extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'skills';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'percentage'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|min:3',
        'percentage' => 'required|numeric|min:1',
    ];

    
}
