<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Portfolio
 * @package App\Models
 * @version August 17, 2022, 1:41 am UTC
 *
 */
class Portfolio extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'portfolios';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'caption',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function portfolioImage()
    {
        return $this->morphMany(Attachment::class, 'attachable')
            ->where('file_attachment', 'portfolio_images');
    }
    
}
