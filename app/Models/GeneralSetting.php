<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class GeneralSetting
 * @package App\Models
 * @version August 5, 2022, 1:15 am UTC
 *
 */
class GeneralSetting extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'general_settings';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'phone',
        'headquarter',
        'about_me',
        'linkedin_url',
        'facebook_url',
        'instagram_url',
        'youtube_url',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
      'name'    => 'required|string|min:3',
      'email'   => 'required|string|min:3',
      'phone'   => 'required|string',
    ];

    public function firstFrontBanner()
    {
        return $this->morphOne(Attachment::class, 'attachable')
            ->fileAttachment('first_front_banner');
    }

    public function secondFrontBanner()
    {
        return $this->morphOne(Attachment::class, 'attachable')
            ->fileAttachment('second_front_banner');
    }

    public function cvDocument()
    {
        return $this->morphOne(Attachment::class, 'attachable')
            ->fileAttachment('cv_document');
    }

    public function portfolioDocument()
    {
        return $this->morphOne(Attachment::class, 'attachable')
            ->fileAttachment('portfolio_document');
    }
    
}
