<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class MyExperience
 * @package App\Models
 * @version August 5, 2022, 2:05 am UTC
 *
 */
class MyExperience extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'my_experiences';
    

    protected $dates = [
        'deleted_at',
        'start_date',
        'end_date',
    ];



    public $fillable = [
        'name',
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
