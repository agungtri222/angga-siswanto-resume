<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class MyExpertise
 * @package App\Models
 * @version August 5, 2022, 1:59 am UTC
 *
 */
class MyExpertise extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'my_expertises';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'caption',
        'icon_name',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|min:3',
        'caption' => 'required|string|min:5',
        'icon_name' => 'required|string',
    ];

    
}
