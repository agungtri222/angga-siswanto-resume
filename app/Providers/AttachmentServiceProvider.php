<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AttachmentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $fileAttachments = config('attachment.file_attachments');

        foreach ($fileAttachments as $name => $fileAttachment) {
            $this->app->singleton($name, $fileAttachment);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
