<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Models\GeneralSetting;
use App\Models\MyExperience;
use App\Models\MyExpertise;
use App\Models\Skill;
use App\Models\Education;
use App\Models\Testimonial;
use App\Models\Portfolio;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // register model polymorphic alias
        Relation::morphMap([
            'general_setting'   => GeneralSetting::class,
            'testimonial'       => Testimonial::class,
            'portfolio'         => Portfolio::class,
        ]);
    }
}
