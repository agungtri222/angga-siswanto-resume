// set dropzone globally
window.Dropzone = require('dropzone').default;

// disable dropzone auto discover to prevent it from declared twice
Dropzone.autoDiscover = false;

window.httpClient = require('./httpClient').default;

