window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

// set nprogress globally
import NProgress from 'nprogress';

const httpClient = axios.create({
    baseUrl: '/',
    timeout: 1000,
    headers: {
        common: {
            'X-Requested-With': 'XMLHttpRequest',
            Accept: 'application/json'
        }
    }
});

const token = document.head.querySelector('meta[name="csrf-token"]');

if(token) {
    httpClient.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

httpClient.interceptors.request.use(
    config => {
        NProgress.start();

        return config;
    },
    error => {
        NProgress.done();

        return Promise.reject(error);
    }
);

httpClient.interceptors.response.use(
    response => {
        NProgress.done();

        return response;
    },
    error => {
        NProgress.done();

        return Promise.reject(error);
    }
);

export default httpClient;