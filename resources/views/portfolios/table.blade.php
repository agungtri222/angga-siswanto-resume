<div class="table-responsive">
    <table class="table" id="portfolios-table">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Caption</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($portfolios as $portfolio)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $portfolio->name }}</td>
                <td>{{ $portfolio->caption }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['portfolios.destroy', $portfolio->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('portfolios.show', [$portfolio->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('portfolios.edit', [$portfolio->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn
                        btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>