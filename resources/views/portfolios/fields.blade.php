<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('name', 'Name*') !!}
        {!! Form::text(
        'name',
        null,
        [
        'class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : ''),
        'maxlength' => '100',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        {!! Form::label('quotcaptione', 'Caption*') !!}
        {!! Form::text(
        'caption',
        null,
        [
        'class' => 'form-control ' . ($errors->has('caption') ? 'is-invalid' : ''),
        'maxlength' => '100',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('caption')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    @include('portfolios.image')
</div>