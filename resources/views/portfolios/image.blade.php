@component('components.uploader', [
'name' => 'portfolio',
'label' => 'Gambar',
'removeable' => true,
'maxFiles' => 5,
'extensions' => '.jpg,.png.jpeg',
'attachments' => isset($portfolio) ? ($portfolio->portfolioImage()->exists() ? [$portfolio->portfolioImage] : []) : [],
'attachable_type' => 'portfolio',
'attachable_id' => isset($portfolio) ? $portfolio->id : '',
'file_attachment' => 'portfolio_images'
])
<p>
    Ekstensi diperbolehkan (png) <br>
    Menerima 1 berkas <br> Maks. ukuran berkas 2MB <br>
</p>
@endcomponent