<div class="form-group">
    <label for="{{ $name }}" class="col-md-3 control-label">{{ $label }}</label>
    <div class="col-md-9">
        <div class="dropzone-container">
            <div id="{{ $name }}" class="dropzone" data-toggle="tooltip" data-html="true" title="{{ $slot }}">
                <div class="dz-message" data-dz-message>
                    <b>Letakkan berkas disini untuk mengunggah</b>
                </div>
            </div>
        </div>
        <div class="dropzone-button-container">
            <button type="button" class="btn btn-info hint" data-toggle="tooltip" data-html="true" title="{{ $slot }}">
                <i class="fa fa-info-circle"></i>
            </button>
            <button type="button" id="{{ $name }}_button" class="btn btn-primary upload">
                <i class="fa fa-upload"></i> Unggah
            </button>
            <br>
            <div>
                @if ($errors->has("{$name}_ids"))
                <span class="help-block text-red">
                    <strong>{{ $errors->first("{$name}_ids") }}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>
</div>
@push('page_scripts')
<script>
    $(function () {
        // set attachment manager globally
        var attachmentManager = {
            bindOpenInNewTab(file, url) {
                // unbind default click handler
                $(file.previewElement).unbind('click');

                // on thumbnail click create url from file
                $(file.previewElement).on('click', function () {
                    window.open(url, '_blank');
                });
            },
            getIcon(file) {
                // get the last word after dot
                let ext = file.name.split('.').pop().toLowerCase();

                let icon = 'txt';

                if (ext === 'docx' || ext === 'doc') {
                    icon = 'doc';
                } else if (ext === 'xlsx' || ext === 'xls') {
                    icon = 'xls';
                } else if (ext === 'pdf') {
                    icon = 'pdf';
                } else if (ext === 'csv') {
                    icon = 'csv';
                }

                return `/storage/images/icons/${icon}.svg`;
            },
            update(dropzone, uploadedFiles, uploadInputs) {
                uploadedFiles.content.forEach(attachment => {
                    // define the mock file
                    if (Array.isArray(attachment)) {
                        attachment.forEach(file => {
                            const mockFile = {
                                id: file.id,
                                name: file.name,
                                size: file.size,
                                type: file.type,
                                dataURL: file.url || file.dataURL,
                                accepted: true
                            };

                            // trigger addedfile event by mock file
                            dropzone.emit('addedfile', mockFile);

                            // determine whether file is image
                            if (mockFile.type.match(/image*/)) {
                                // download image then resize it to create the thumbnail
                                dropzone.createThumbnailFromUrl(
                                    mockFile,
                                    dropzone.options.thumbnailWidth,
                                    dropzone.options.thumbnailHeight,
                                    dropzone.options.thumbnailMethod,
                                    true,
                                    thumbnail => dropzone.emit('thumbnail', mockFile, thumbnail)
                                );
                            } else {
                                // get icon to create the thumbnail
                                dropzone.emit('thumbnail', mockFile, this.getIcon(mockFile));
                            }

                            // trigger complete event by mock file
                            dropzone.emit('complete', mockFile);

                            // this line needed to ensure max files exceeded event is working
                            dropzone.files.push(mockFile);

                            // create a new input hidden with value is mock file's id
                            uploadInputs.create(mockFile.id);

                            this.bindOpenInNewTab(mockFile, mockFile.dataURL);
                        });
                    } else {
                        const mockFile = {
                            id: attachment.id,
                            name: attachment.name,
                            size: attachment.size,
                            type: attachment.type,
                            dataURL: attachment.url || attachment.dataURL,
                            accepted: true
                        };

                        // trigger addedfile event by mock file
                        dropzone.emit('addedfile', mockFile);

                        // determine whether file is image
                        if (mockFile.type.match(/image*/)) {
                            // download image then resize it to create the thumbnail
                            dropzone.createThumbnailFromUrl(
                                mockFile,
                                dropzone.options.thumbnailWidth,
                                dropzone.options.thumbnailHeight,
                                dropzone.options.thumbnailMethod,
                                true,
                                thumbnail => dropzone.emit('thumbnail', mockFile, thumbnail)
                            );
                        } else {
                            // get icon to create the thumbnail
                            dropzone.emit('thumbnail', mockFile, this.getIcon(mockFile));
                        }

                        // trigger complete event by mock file
                        dropzone.emit('complete', mockFile);

                        // this line needed to ensure max files exceeded event is working
                        dropzone.files.push(mockFile);

                        // create a new input hidden with value is mock file's id
                        uploadInputs.create(mockFile.id);

                        this.bindOpenInNewTab(mockFile, mockFile.dataURL);
                    }
                });
            }
        };

        window['{{ $name }}'] = {
          
            files: {
                content: [],
                create(attachment) {
                    this.content.push(attachment);
                    return this;
                },
                find(id) {
                    return this.content.filter(item => item.id === id);
                },
                delete(id) {
                    this.content.forEach((item, index, arr) => {
                        if (item.id === id) {
                            arr.splice(index, 1);
                        }
                    });
                    return this;
                },
                store() {
                    sessionStorage.setItem('{{ $name }}', JSON.stringify(this.content));
                },
                fetch() {
                    return JSON.parse(sessionStorage.getItem('{{ $name }}'));
                }
            },
            button: {
                el: $('#{{ $name }}_button'),
                set(el) {
                    this.el = el;
                },
                show() {
                    this.el.show()
                },
                hide() {
                    this.el.hide()
                },
                registerConfirmation(dropzone) {
                    let el = this.el;
                    el.on('click', function () {
                        // if confirmed upload all files in queue
                        dropzone.processQueue();
                        // hide button
                        el.hide();
                    });
                }
            },
            inputs: {
                dropzoneEl: $('#{{ $name }}'),
                create(value) {
                    this.dropzoneEl.append(
                        $('<input>', {
                            type: 'hidden',
                            name: '{{ $name }}_ids[]',
                            value: value
                        })
                    );
                },
                delete(id) {
                    this.dropzoneEl.find('input').each(function () {
                        // remove input if file id equals to input value
                        if (parseInt($(this).val()) === id) {
                            $(this).remove();
                        }
                    });
                }
            }
        };
        
        window['{{ $name }}'].dropzone = new Dropzone('#{{ $name }}', {
            url: '{{ route('attachments.store') }}',
            addRemoveLinks: true,
            acceptedFiles: @json($extensions), // allowed file extensions with comma as delimiter
            maxFilesize: @json($maxFileSize ?? 10), // in mb
            maxFiles: @json($maxFiles ?? 1),
            parallelUploads: @json($maxFiles ?? 1),
            autoProcessQueue: false,
            paramName: 'attachment', // request key for uploaded file
            params: {
                attachable_type: @json($attachable_type),
                attachable_id: @json($attachable_id),
                file_attachment: @json($file_attachment)
            },
            headers: {
                accept: 'application/json',
                'X-CSRF-Token': document.head.querySelector('meta[name="csrf-token"]').content
            },
            init() {
                const dropzone = this;

                // running on request validation error
                if (@json($errors->any())) {
                    // fetch uploaded files from session storage
                    const attachments = window['{{ $name }}'].files.fetch();
                    if (attachments !== null) {
                        // if attachments exists then sets it to uploaded files
                        window['{{ $name }}'].files.content = attachments;
                        attachmentManager.update(dropzone, window['{{ $name }}'].files, window['{{ $name }}'].inputs);
                    }
                } else if (@json($attachments)) {
                    // fill attachments
                    window['{{ $name }}'].files.content = @json($attachments);
                    window['{{ $name }}'].files.store();
                    attachmentManager.update(dropzone, window['{{ $name }}'].files, window['{{ $name }}'].inputs);
                }

                window['{{ $name }}'].button.registerConfirmation(dropzone);

                this.on('addedfile', function (file) {
                    // attach thumbnail to file's preview element's image
                    $(file.previewElement).find('.dz-image img').attr('src', attachmentManager.getIcon(file));
                    attachmentManager.bindOpenInNewTab(file, URL.createObjectURL(file));
                    window['{{ $name }}'].button.show();
                });

                this.on('success', function (file, response) {
                    const { id, name, size, type, url } = response.data;
                    // this line is needed to delete file by id purpose
                    file.id = id;
                    // fill uploaded files arr with data's attribute
                    const attachment = {
                        id,
                        name,
                        size,
                        type,
                        dataURL: url
                    };
                    // append a new input hidden with value is data's id
                    window['{{ $name }}'].inputs.create(id);
                    window['{{ $name }}'].files.create(attachment).store();
                    attachmentManager.bindOpenInNewTab(file, url);
                });

                this.on('removedfile', function (file) {
                    const id = file.id;

                    // get uploaded file from uploaded files arr
                    const attachment = window['{{ $name }}'].files.find(id);

                    // if file exists in uploaded files arr
                    // then deletes it from server
                    if (attachment) {
                        httpClient.delete(`/attachments/${id}`)
                            .then(response => {
                                console.log(response)
                                window['{{ $name }}'].files.delete(id).store();

                                window['{{ $name }}'].inputs.delete(id);
                            })
                            .catch(error => {
                                throw error
                            });
                    }

                    if (!dropzone.files.filter(file => !file.accepted).length) {
                        window['{{ $name }}'].button.show();
                    } else {
                        window['{{ $name }}'].button.hide();
                    }

                    // if this doesnt have queued files hide upload button
                    if (!dropzone.getQueuedFiles().length) {
                        window['{{ $name }}'].button.hide();
                    }
                });

                this.on('error', function (file, error) {
                    let message = error.message;
                    // get laravel's errors message bag
                    const errors = error.errors;
                    // if errors isn't undefined process it
                    if (errors) {
                        // if exists then looping foreach errors properties
                        for (let property in errors) {
                            // determine whether errors has the following property
                            if (errors.hasOwnProperty(property)) {
                                // get the first errors message by property then set it to message
                                message = errors[property][0];
                                // break the looping on first errors message encounters
                                break;
                            }
                        }
                    }
                    // display error message on dropzone
                    $(file.previewElement).find('.dz-error-message').text(message);

                    window['{{ $name }}'].button.hide();
                });
            }
        });
    });
</script>
@endpush