@component('components.uploader', [
'name' => 'second_front_banner',
'label' => 'Banner Kedua',
'removeable' => true,
'extensions' => '.jpg,.png.jpeg',
'attachments' => isset($generalSetting) ? ($generalSetting->secondFrontBanner()->exists() ?
[$generalSetting->secondFrontBanner] : []) :
[],
'attachable_type' => 'general_setting',
'attachable_id' => isset($generalSetting) ? $generalSetting->id : '',
'file_attachment' => 'second_front_banner'
])
Ekstensi diperbolehkan (png)
Menerima 1 berkas
Maks. ukuran berkas 2MB
@endcomponent