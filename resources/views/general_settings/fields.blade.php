<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('name', 'Name*') !!}
        {!! Form::text(
        'name',
        null,
        [
        'class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : ''),
        'maxlength' => '50',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email*') !!}
        {!! Form::text(
        'email',
        null,
        [
        'class' => 'form-control ' . ($errors->has('email') ? 'is-invalid' : ''),
        'maxlength' => '50',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('email')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        {!! Form::label('phone', 'Phone*') !!}
        {!! Form::text(
        'phone',
        null,
        [
        'class' => 'form-control ' . ($errors->has('phone') ? 'is-invalid' : ''),
        'maxlength' => '20',
        'required'
        ]
        ) !!}
        @error('phone')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        {!! Form::label('headquarter', 'Headquarter*') !!}
        {!! Form::text(
        'headquarter',
        null,
        [
        'class' => 'form-control ' . ($errors->has('headquarter') ? 'is-invalid' : ''),
        'maxlength' => '20',
        'required'
        ]
        ) !!}
        @error('headquarter')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        {!! Form::label('about_me', 'About Me*') !!}
        {!! Form::textarea(
        'about_me',
        null,
        [
        'class' => 'form-control ' . ($errors->has('about_me') ? 'is-invalid' : ''),
        'maxlength' => '20',
        'required'
        ]
        ) !!}
        @error('about_me')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        {!! Form::label('linkedin_url', 'LinkedIn') !!}
        {!! Form::text(
        'linkedin_url',
        null,
        [
        'class' => 'form-control ' . ($errors->has('linkedin_url') ? 'is-invalid' : ''),
        'maxlength' => '20',
        'required'
        ]
        ) !!}
        @error('linkedin_url')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        {!! Form::label('facebook_url', 'Facebook') !!}
        {!! Form::text(
        'facebook_url',
        null,
        [
        'class' => 'form-control ' . ($errors->has('facebook_url') ? 'is-invalid' : ''),
        'maxlength' => '20',
        'required'
        ]
        ) !!}
        @error('facebook_url')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        {!! Form::label('instagram_url', 'Instagram') !!}
        {!! Form::text(
        'instagram_url',
        null,
        [
        'class' => 'form-control ' . ($errors->has('instagram_url') ? 'is-invalid' : ''),
        'maxlength' => '20',
        'required'
        ]
        ) !!}
        @error('instagram_url')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    @include('general_settings.image')
    @include('general_settings.second_banner')
    @include('general_settings.cv_document')
    @include('general_settings.portfolio_document')
</div>