<div class="col-md-6">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $generalSetting->name }}</p>
</div>

<div class="col-md-6">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $generalSetting->email }}</p>
</div>

<div class="col-md-6">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $generalSetting->phone }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $generalSetting->created_at }}</p>
</div>

<div class="col-md-6">
    {!! Form::label('facebook_url', 'Facebook:') !!}
    <p>{{ $generalSetting->facebook_url }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $generalSetting->updated_at }}</p>
</div>

<div class="col-md-6">
    {!! Form::label('linkedin_url', 'LinkedIn:') !!}
    <p>{{ $generalSetting->linkedin_url }}</p>
</div>

<div class="col-md-6">
    {!! Form::label('instagram_url', 'Instagram:') !!}
    <p>{{ $generalSetting->instagram_url }}</p>
</div>

<div class="col-md-6">
    {!! Form::label('youtube_url', 'Youtube:') !!}
    <p>{{ $generalSetting->youtube_url }}</p>
</div>