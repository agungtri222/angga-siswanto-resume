<div class="table-responsive">
    <table class="table" id="generalSettings-table">
        <thead>
            <tr>

                <th>Name</th>
                <th>E-mail</th>
                <th>Phone</th>
                <th>Headquarter</th>
                <th>LinkedIn</th>
                <th>Facebook</th>
                <th>Instagram</th>
                <th>Youtube</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($generalSettings as $generalSetting)
            <tr>
                <td>{{ $generalSetting->name }}</td>
                <td>{{ $generalSetting->email }}</td>
                <td>{{ $generalSetting->phone }}</td>
                <td>{{ $generalSetting->headquarter }}</td>
                <td>{{ $generalSetting->linkedin_url }}</td>
                <td>{{ $generalSetting->facebook_url }}</td>
                <td>{{ $generalSetting->instagram_url }}</td>
                <td>{{ $generalSetting->youtube_url }}</td>

                <td width="120">
                    {!! Form::open(['route' => ['generalSettings.destroy', $generalSetting->id], 'method' => 'delete'])
                    !!}
                    <div class='btn-group'>
                        <a href="{{ route('generalSettings.show', [$generalSetting->id]) }}"
                            class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('generalSettings.edit', [$generalSetting->id]) }}"
                            class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn
                        btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>