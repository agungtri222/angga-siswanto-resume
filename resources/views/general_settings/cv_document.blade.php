@component('components.uploader', [
'name' => 'cv_document',
'label' => 'CV',
'removeable' => true,
'extensions' => '.pdf',
'attachments' => isset($generalSetting) ? ($generalSetting->cvDocument()->exists() ?
[$generalSetting->cvDocument] : []) :
[],
'attachable_type' => 'general_setting',
'attachable_id' => isset($generalSetting) ? $generalSetting->id : '',
'file_attachment' => 'cv_document'
])
Ekstensi diperbolehkan (pdf).
Menerima 1 berkas.
Maks. ukuran berkas 2MB
@endcomponent