@component('components.uploader', [
'name' => 'portfolio_document',
'label' => 'Portfolio',
'removeable' => true,
'extensions' => '.pdf,.docx',
'attachments' => isset($generalSetting) ? ($generalSetting->portfolioDocument()->exists() ?
[$generalSetting->portfolioDocument] : []) :
[],
'attachable_type' => 'general_setting',
'attachable_id' => isset($generalSetting) ? $generalSetting->id : '',
'file_attachment' => 'portfolio_document'
])
Ekstensi diperbolehkan (pdf).
Menerima 1 berkas.
Maks. ukuran berkas 4MB
@endcomponent