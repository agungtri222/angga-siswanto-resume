@component('components.uploader', [
'name' => 'front_banner',
'label' => 'Banner Pertama',
'removeable' => true,
'extensions' => '.jpg,.png.jpeg',
'attachments' => isset($generalSetting) ? ($generalSetting->firstFrontBanner()->exists() ?
[$generalSetting->firstFrontBanner] : []) :
[],
'attachable_type' => 'general_setting',
'attachable_id' => isset($generalSetting) ? $generalSetting->id : '',
'file_attachment' => 'first_front_banner'
])
Ekstensi diperbolehkan (png)
Menerima 1 berkas
Maks. ukuran berkas 2MB
@endcomponent