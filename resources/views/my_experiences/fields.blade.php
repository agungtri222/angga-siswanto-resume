<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('name', 'Name*') !!}
        {!! Form::text(
        'name',
        null,
        [
        'class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : ''),
        'maxlength' => '50',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        {!! Form::label('start_date', 'Start Date*') !!}
        {!! Form::date(
        'start_date',
        null,
        [
        'class' => 'form-control ' . ($errors->has('start_date') ? 'is-invalid' : ''),
        'maxlength' => '50',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('start_date')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        {!! Form::label('end_date', 'End Date*') !!}
        {!! Form::date(
        'end_date',
        null,
        [
        'class' => 'form-control ' . ($errors->has('end_date') ? 'is-invalid' : ''),
        'maxlength' => '50',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('end_date')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>