<div class="table-responsive">
    <table class="table" id="myExperiences-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($myExperiences as $myExperience)
            <tr>
                <td>{{ $myExperience->name }}</td>
                <td>{{ $myExperience->start_date->format('d M Y') }}</td>
                <td>{{ $myExperience->end_date->format('d M Y') }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['myExperiences.destroy', $myExperience->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('myExperiences.show', [$myExperience->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('myExperiences.edit', [$myExperience->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn
                        btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>