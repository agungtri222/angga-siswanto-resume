<div class="table-responsive">
    <table class="table" id="education-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Degree</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($education as $education)
            <tr>
                <td>{{ $education->name }}</td>
                <td>{{ $education->degree }}</td>
                <td>{{ $education->start_date->format('Y') }}</td>
                <td>{{ $education->end_date->format('Y') }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['education.destroy', $education->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('education.show', [$education->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('education.edit', [$education->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn
                        btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>