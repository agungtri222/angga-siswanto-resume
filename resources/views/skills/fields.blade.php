<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('name', 'Name*') !!}
        {!! Form::text(
        'name',
        null,
        [
        'class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : ''),
        'maxlength' => '50',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('percentage', 'Percentage*') !!}
        {!! Form::number(
        'percentage',
        null,
        [
        'class' => 'form-control ' . ($errors->has('percentage') ? 'is-invalid' : ''),
        'maxlength' => '50',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('percentage')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>