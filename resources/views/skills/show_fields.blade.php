<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $skill->name }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('percentage', 'Percentage:') !!}
    <p>{{ $skill->percentage }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $skill->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $skill->updated_at }}</p>
</div>