<div class="table-responsive">
    <table class="table" id="myExpertises-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Caption</th>
                <th>Icon</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($myExpertises as $myExpertise)
            <tr>
                <td>{{ $myExpertise->name }}</td>
                <td>{{ $myExpertise->caption }}</td>
                <td><i class="fas fa-{{$myExpertise->icon_name}}"></i></td>
                <td width="120">
                    {!! Form::open(['route' => ['myExpertises.destroy', $myExpertise->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('myExpertises.show', [$myExpertise->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('myExpertises.edit', [$myExpertise->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn
                        btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>