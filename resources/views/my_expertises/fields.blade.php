<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('name', 'Name*') !!}
        {!! Form::text(
        'name',
        null,
        [
        'class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : ''),
        'maxlength' => '100',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        {!! Form::label('caption', 'Caption*') !!}
        {!! Form::textarea(
        'caption',
        null,
        [
        'class' => 'form-control ' . ($errors->has('caption') ? 'is-invalid' : ''),
        'maxlength' => '255',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('caption')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        {!! Form::label('icon_name', 'Icon Name*') !!}
        {!! Form::text(
        'icon_name',
        null,
        [
        'class' => 'form-control ' . ($errors->has('icon_name') ? 'is-invalid' : ''),
        'maxlength' => '50',
        'minlength' => '3',
        'required'
        ]
        ) !!}
        @error('icon_name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>