<li class="nav-item">
    <a href="{{ route('generalSettings.index') }}"
       class="nav-link {{ Request::is('generalSettings*') ? 'active' : '' }}">
        <p>General Settings</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('myExpertises.index') }}"
       class="nav-link {{ Request::is('myExpertises*') ? 'active' : '' }}">
        <p>My Expertises</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('myExperiences.index') }}"
       class="nav-link {{ Request::is('myExperiences*') ? 'active' : '' }}">
        <p>My Experiences</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('education.index') }}"
       class="nav-link {{ Request::is('education*') ? 'active' : '' }}">
        <p>Education</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('skills.index') }}"
       class="nav-link {{ Request::is('skills*') ? 'active' : '' }}">
        <p>Skills</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('testimonials.index') }}"
       class="nav-link {{ Request::is('testimonials*') ? 'active' : '' }}">
        <p>Testimonials</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('portfolios.index') }}"
       class="nav-link {{ Request::is('portfolios*') ? 'active' : '' }}">
        <p>Portfolios</p>
    </a>
</li>


