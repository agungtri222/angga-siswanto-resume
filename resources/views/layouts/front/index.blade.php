<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>

    @include('layouts.front.head')

</head>

<body class="stretched sticky-responsive-menu">

    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
		============================================= -->
        <header id="header" class="transparent-header sticky-transparent static-sticky">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
					============================================= -->
                    <div id="logo">
                        <a href="{{ route('front') }}" class="standard-logo font-secondary ls3"
                            style="line-height: 90px;">canvas</a>
                        <a href="{{ route('front') }}" class="retina-logo font-secondary ls3"
                            style="line-height: 90px;">canvas</a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
					============================================= -->
                    <nav id="primary-menu">

                        <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="0">
                            <li class="current"><a href="#" data-href="#wrapper"><i class="icon-line2-home"></i>
                                    <div>Intro</div>
                                </a></li>
                            <li><a href="#" data-href="#section-skills"><i class="icon-line2-star"></i>
                                    <div>Skills</div>
                                </a></li>
                            <li><a href="#" data-href="#section-about"><i class="icon-line2-user"></i>
                                    <div>About</div>
                                </a></li>
                            <li><a href="#" data-href="#section-works"><i class="icon-line2-grid"></i>
                                    <div>Portfolio</div>
                                </a></li>
                            <li><a href="#" data-href="#section-articles"><i class="icon-line2-pencil"></i>
                                    <div>Testimonails</div>
                                </a></li>
                            <li><a href="#" data-href="#footer"><i class="icon-line2-envelope"></i>
                                    <div>Contact</div>
                                </a></li>
                        </ul>

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

        <section id="slider" class="slider-element full-screen force-full-screen clearfix">
            <div class="full-screen force-full-screen" style="position: fixed; width: 100%; background: #FFF url('{{ $generalSetting->firstFrontBanner->url ?? '' }}')
                no-repeat top center; background-size: cover; background-attachment: fixed;">

                <div class="container clearfix">
                    <div class="slider-caption dark slider-caption-right">
                        <h2 class="font-primary ls5" data-animate="fadeIn">{{ $generalSetting->name }}</h2>
                        <p class="t300 ls1 d-none d-sm-block" data-animate="fadeIn" data-delay="400">Simple Design
                            Methodology.<br>Melbourn, Australia.</p>
                        <a class="font-primary noborder ls1 topmargin-sm inline-block more-link text-white dark d-none d-sm-inline-block"
                            data-animate="fadeIn" data-delay="800" data-scrollto="#section-works" data-offset="0"
                            href="#"><u>My Works</u> &rarr;</a>
                    </div>
                </div>

            </div>
            <div class="full-screen force-full-screen blurred-img"
                style="position: fixed; width: 100%; top: 0; left: 0; background: #FFF url('{{ $generalSetting->firstFrontBanner->url ?? '' }}') no-repeat top center; background-size: cover; background-attachment: fixed;">
            </div>
        </section>

        <!-- Content
		============================================= -->
        <section id="content" class="nobg">

            <div class="content-wrap nobottompadding nobg">

                <div id="section-skills" class="section nomargin page-section dark nobg clearfix"
                    style="padding-bottom: 50px">
                    <div class="container clearfix">
                        <div class="heading-block">
                            <h2 class="font-secondary center">My Expertises.</h2>
                        </div>

                        <div class="col_one_third">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[0]['icon_name'] }}"
                                            style="color:#DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[0]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[0]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                        <div class="col_one_third">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[1]['icon_name'] ?? 'user' }}"
                                            style="color: #DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[1]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[1]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                        <div class="col_one_third col_last">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[2]['icon_name'] ?? 'user' }}"
                                            style="color: #DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[2]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[2]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                        <div class="clear"></div>

                        <div class="col_one_third">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[3]['icon_name'] ?? 'user' }}"
                                            style="color: #DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[3]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[3]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                        <div class="col_one_third">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[4]['icon_name'] ?? 'user' }}"
                                            style="color: #DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[4]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[4]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                        <div class="col_one_third col_last">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[5]['icon_name'] ?? 'user' }}"
                                            style="color: #DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[5]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[5]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                        <div class="clear"></div>

                        <div class="col_one_third">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[6]['icon_name'] ?? 'user' }}"
                                            style="color: #DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[6]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[6]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                        <div class="col_one_third">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[7]['icon_name'] ?? 'user' }}"
                                            style="color: #DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[7]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[7]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                        <div class="col_one_third col_last">
                            <div class="feature-box fbox-plain">
                                <div class="fbox-icon">
                                    <a href="#"><i class="fas fa-{{ $myExpertises[8]['icon_name'] ?? 'user' }}"
                                            style="color: #DDD"></i></a>
                                </div>
                                <h3 class="t400 ls2" style="color: #FFF">{{ $myExpertises[8]['name'] ?? '' }}</h3>
                                <p style="color:#FFF;">{{ $myExpertises[8]['caption'] ?? '' }}</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="section-about" class="section page-section nomargin clearfix"
                    style="background: #EEE url('{{ $generalSetting->secondFrontBanner->url ?? '' }}') no-repeat center center; background-size: cover; padding: 100px 0">
                    <div class="container clearfix">
                        <div class="row clearfix">
                            <div class="col-md-5 offset-md-7 clearfix">
                                <div class="heading-block">
                                    <h2 class="font-secondary">About Me.</h2>
                                    <span>{{ $generalSetting->about_me }}</span>
                                </div>
                                <table class=" table">
                                    <tbody>
                                        <tr>
                                            <td class="notopborder"><strong>Name:</strong></td>
                                            <td class="notopborder">{{ $generalSetting->name }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Gender:</strong></td>
                                            <td>Male</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Email:</strong></td>
                                            <td>{{ $generalSetting->email }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Phone:</strong></td>
                                            <td>{{ $generalSetting->phone }}</td>
                                        </tr>
                                        {{-- <tr>
                                            <td><strong>Website:</strong></td>
                                            <td>semicolonweb.com</td>
                                        </tr> --}}
                                        {{-- <tr>
                                            <td><strong>DOB:</strong></td>
                                            <td>6th September 1986</td>
                                        </tr> --}}
                                        <tr>
                                            <td><strong>Nationality:</strong></td>
                                            <td>Indonesian</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-md-5">
                                        <a href="{{ $generalSetting->cvDocument()->url ?? '' }}"
                                            class="button button-large button-border button-black button-dark noleftmargin"
                                            target="_blank"><i class="icon-line-cloud-download"></i> Download CV</a>
                                    </div>
                                    <div class="col-md-5">
                                        <a href="{{ $generalSetting->portfolioDocument()->url ?? '' }}"
                                            class="button button-large button-border button-black button-dark noleftmargin"
                                            target="_blank"><i class="icon-line-cloud-download"></i> Download
                                            Portfolio</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="video-wrap">
                        <div class="video-overlay d-sm-block d-md-none" style="background: rgba(255,255,255,0.9);">
                        </div>
                    </div>
                </div>

                <div class="section nomargin skill-area bgcolor dark clearfix" style="padding: 80px 0;">
                    <div class="container clearfix">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-6">
                                <h4>Education</h4>
                                <div class="skill-info">
                                    @foreach($educations as $education)
                                    <span>
                                        {{ $education->name }}<br>
                                        {{ $education->degree }}<br>
                                        {{ $education->start_date->format('Y') }} - {{ $education->end_date->format('Y')
                                        }}
                                    </span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="w-100 bottommargin d-block d-md-none"></div>

                            <div class="col-lg-4 col-md-6">
                                <h4>Experience</h4>
                                <div class="skill-info">
                                    @foreach($myExperiences as $experience)
                                    <span>
                                        {{ $experience->start_date->format('Y') }} - {{
                                        $experience->end_date->format('Y') }}<br>
                                        {{ $experience->name }}
                                    </span>
                                    @endforeach
                                </div>
                            </div>

                            <div class="w-100 bottommargin d-block d-lg-none clear"></div>

                            <div class="col-lg-4 col-12">
                                <h4>Skills</h4>
                                <ul class="skills">
                                    @foreach($skills as $skill)
                                    <li data-percent="{{ $skill->percentage }}">
                                        <span>{{ $skill->name }}</span>
                                        <div class="progress">
                                            <div class="progress-percent">
                                                <div class="counter counter-inherit counter-instant"><span data-from="0"
                                                        data-to="{{ $skill->percentage }}" data-refresh-interval="30"
                                                        data-speed="1100"></span>%</div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>

                <section id="content">

                    <div class="content-wrap" id="section-works">

                        <div class="container clearfix">

                            <!-- Portfolio Filter
                            ============================================= -->
                            <ul class="portfolio-filter clearfix" data-container="#portfolio">

                                <li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
                                @foreach ($portfolios as $portfolio)
                                <li><a href="#" data-filter=".{{ \Str::slug($portfolio->name) }}">{{ $portfolio->name
                                        }}</a></li>
                                @endforeach

                            </ul><!-- #portfolio-filter end -->

                            <div class="portfolio-shuffle" data-container="#portfolio">
                                <i class="icon-random"></i>
                            </div>

                            <div class="clear"></div>

                            <!-- Portfolio Items
                            ============================================= -->
                            <div id="portfolio" class="portfolio grid-container portfolio-3 clearfix">

                                @foreach ($portfolios as $portfolio)
                                <article class="portfolio-item {{ \Str::slug($portfolio->name) }}">
                                    <div class="portfolio-image">
                                        <div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
                                            <div class="flexslider">
                                                <div class="slider-wrap">
                                                    @foreach($portfolio->portfolioImage as $image)
                                                    <div class="slide">
                                                        <a href="#">
                                                            <img src="{{ $image->url }}" alt="Morning Dew">
                                                        </a>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        @foreach($portfolio->portfolioImage as $image)
                                        <div class="portfolio-overlay" data-lightbox="gallery">
                                            <a href="{{ $image->url }}" data-lightbox="gallery-item" class="left-icon">
                                                <i class="icon-line-stack-2"></i>
                                            </a>
                                            <a href="{{ $image->url }}" class="left-icon"
                                                data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
                                            <a href="{{ $image->url }}" class="hidden" data-lightbox="gallery-item"></a>
                                            <a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="portfolio-desc">
                                        <h3><a href="portfolio-single-gallery.html">{{ $portfolio->name }}</a></h3>
                                        <span><a href="#">{{ $portfolio->caption }}</a></span>
                                    </div>
                                </article>
                                @endforeach

                            </div><!-- #portfolio end -->

                        </div>

                    </div>

                </section><!-- #content end -->

                <div id="section-articles" class="dark nobottommargin bgcolor"
                    style="background-image: url('images/services/home-testi-bg.jpg'); padding: 100px 0;">

                    <div class="heading-block center">
                        <h3>What People Says About Me?</h3>
                    </div>

                    <section id="slider" class="slider-element boxed-slider">

                        <div class="container clearfix">

                            <div class="fslider" data-easing="easeInQuad">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        @foreach($testimonials as $testimonial)
                                        <div class="slide" data-thumb="{{ $testimonial->testimonialImage->url }}">
                                            <a href="#">
                                                <img src="{{ $testimonial->testimonialImage->url }}"
                                                    alt="{{ $testimonial->name }}">
                                                <div class="flex-caption slider-caption-bg">{{ $testimonial->quote }}
                                                </div>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>

                    </section>
                </div>

        </section><!-- #content end -->

        <!-- Footer
		============================================= -->
        <footer id="footer" class="page-section dark noborder nopadding clearfix" style="background-color: #1C1C1C;">

            <div class="container clearfix">

                <!-- Footer Widgets
				============================================= -->
                <div class="footer-widgets-wrap clearfix" style="padding: 80px 0">
                    <div class="col_one_fourth">
                        <div class="footer-logo"><span class="t400 color ls1" style="font-size: 22px; ">{{
                                $generalSetting->name
                                }}</span><br><small class="ls3 uppercase" style="color: rgba(255,255,255,0.2);">&copy;
                                {{ date('Y') }} Reserved.</small></div>
                    </div>
                    <div class="col_three_fourth col_last">
                        <div class="col_one_third">
                            <div class="widget widget_links clearfix">
                                <h4>Contact Us</h4>
                                <div class="footer-content">
                                    <abbr title="Phone Number"><strong>Phone:</strong></abbr> {{ $generalSetting->phone
                                    }}<br>
                                    {{-- <abbr title="Fax"><strong>Fax:</strong></abbr> (91) 11 4752 1433<br> --}}
                                    <abbr title="Email Address"><strong>Email:</strong></abbr> {{
                                    $generalSetting->email }}
                                </div>
                            </div>
                        </div>
                        <div class="col_one_third">
                            <div class="widget clearfix">
                                <h4>Location</h4>
                                <div class="footer-content">
                                    <address>
                                        <strong>Headquarters:</strong><br>
                                        {{-- 795 Folsom Ave, Suite 600<br>
                                        San Francisco, CA 94107<br> --}}
                                        {{ $generalSetting->headquarter }} <br />
                                    </address>
                                </div>
                            </div>
                        </div>
                        <div class="col_one_third col_last">
                            <div class="widget widget_links clearfix">
                                <h4>Social</h4>
                                <a href="{{ $generalSetting->facebook_url }}"
                                    class="social-icon nobg si-small si-light si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                {{-- <a href="{{  }}" class="social-icon nobg si-small si-light si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a> --}}
                                {{-- <a href="#" class="social-icon nobg si-small si-light si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a> --}}
                                <a href="{{ $generalSetting->instagram_url }}"
                                    class="social-icon nobg si-small si-light si-instagram">
                                    <i class="icon-instagram"></i>
                                    <i class="icon-instagram"></i>
                                </a>
                                {{-- <a href="#" class="social-icon nobg si-small si-light si-dribbble">
                                    <i class="icon-dribbble"></i>
                                    <i class="icon-dribbble"></i>
                                </a> --}}

                                <a href="{{ $generalSetting->youtube_url }}"
                                    class="social-icon nobg si-small si-light si-youtube">
                                    <i class="icon-youtube"></i>
                                    <i class="icon-youtube"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Copyrights
			============================================= -->
            <div id="copyrights" style="background-color: #111;">

                <div class="container clearfix">

                    <div class="col_full center nobottommargin">
                        Copyrights &copy; {{ date('Y') }} All Rights Reserved by Canvas Inc.<br>
                        <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
	============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    @include('layouts.front.scripts')

</body>

</html>