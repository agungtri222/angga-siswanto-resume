<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="SemiColonWeb" />

<!-- Stylesheets
============================================= -->
<link
    href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700,900|Playfair+Display:400,700"
    rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('front/css/bootstrap.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('front/css/style.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('front/css/dark.css') }}" type="text/css" />

<!-- Resume Specific Stylesheet -->
<link rel="stylesheet" href="{{ asset('front/css/resume.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('front/css/fonts.css') }}" type="text/css" />
<!-- / -->

<link rel="stylesheet" href="{{ asset('front/css/font-icons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('front/css/animate.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('front/css/magnific-popup.css') }}" type="text/css" />

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
    integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />

<link rel="stylesheet" href="{{ asset('front/css/responsive.css') }}" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<style>
    .dark .testimonial {
        background-color: transparent;
        border: none;
        box-shadow: none;
    }

    .testimonial .flex-control-nav li a {
        background-color: #fff;
    }
</style>

{{--
<link rel="stylesheet" href="css/colors.php?color=7B6ED6" type="text/css" /> --}}

<!-- Document Title
============================================= -->
<title>Resume | Canvas</title>