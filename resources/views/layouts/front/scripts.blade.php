<!-- External JavaScripts
	============================================= -->
<script src="{{ asset('front/js/jquery.js') }}"></script>
<script src="{{ asset('front/js/plugins.js') }}"></script>

<!-- Footer Scripts
	============================================= -->
<script src="{{ asset('front/js/functions.js')}}"></script>

<script>
    jQuery(window).scroll(function() {
        var pixs = jQuery(window).scrollTop(),
            opacity = pixs / 650,
            element = jQuery( '.blurred-img' ),
            elementHeight = element.outerHeight(),
            elementNextHeight = jQuery('.content-wrap').find('.page-section').first().outerHeight();
        if( ( elementHeight + elementNextHeight + 50 ) > pixs ) {
            element.addClass('blurred-image-visible');
            element.css({ 'opacity': opacity });
        } else {
            element.removeClass('blurred-image-visible');
        }
    });
</script>