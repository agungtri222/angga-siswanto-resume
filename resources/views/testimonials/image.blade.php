@component('components.uploader', [
'name' => 'testimonial',
'label' => 'Gambar',
'removeable' => true,
'extensions' => '.jpg,.png.jpeg',
'attachments' => isset($testimonial) ? ($testimonial->testimonialImage()->exists() ?
[$testimonial->testimonialImage] : []) :
[],
'attachable_type' => 'testimonial',
'attachable_id' => isset($testimonial) ? $testimonial->id : '',
'file_attachment' => 'testimonial_image'
])
<p>
    Ekstensi diperbolehkan (png) <br> Menerima 1 berkas <br> Maks. ukuran berkas 2MB <br>
    max. panjang gambar 250px <br> max. tinggi gambar 200px
</p>
@endcomponent